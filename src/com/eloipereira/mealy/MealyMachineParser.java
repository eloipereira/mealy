// $ANTLR 3.4 /Users/eloipereira/Dropbox/Documents/workspace/Mealy/src/com/eloipereira/mealy/MealyMachine.g 2013-10-23 00:05:13

  package com.eloipereira.mealy;
  import java.util.HashMap;


import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

import org.antlr.runtime.tree.*;


@SuppressWarnings({"all", "warnings", "unchecked"})
public class MealyMachineParser extends Parser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "CHAR", "INT", "STRING", "WS", "'('", "')'", "','", "'->'", "';'", "'Initial State = '", "'Input word = '", "'Output Function = {'", "'Transition Function = {'", "'};'"
    };

    public static final int EOF=-1;
    public static final int T__8=8;
    public static final int T__9=9;
    public static final int T__10=10;
    public static final int T__11=11;
    public static final int T__12=12;
    public static final int T__13=13;
    public static final int T__14=14;
    public static final int T__15=15;
    public static final int T__16=16;
    public static final int T__17=17;
    public static final int CHAR=4;
    public static final int INT=5;
    public static final int STRING=6;
    public static final int WS=7;

    // delegates
    public Parser[] getDelegates() {
        return new Parser[] {};
    }

    // delegators


    public MealyMachineParser(TokenStream input) {
        this(input, new RecognizerSharedState());
    }
    public MealyMachineParser(TokenStream input, RecognizerSharedState state) {
        super(input, state);
    }

protected TreeAdaptor adaptor = new CommonTreeAdaptor();

public void setTreeAdaptor(TreeAdaptor adaptor) {
    this.adaptor = adaptor;
}
public TreeAdaptor getTreeAdaptor() {
    return adaptor;
}
    public String[] getTokenNames() { return MealyMachineParser.tokenNames; }
    public String getGrammarFileName() { return "/Users/eloipereira/Dropbox/Documents/workspace/Mealy/src/com/eloipereira/mealy/MealyMachine.g"; }


      private String word = new String(); 


    public static class prog_return extends ParserRuleReturnScope {
        public String outputWord;
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "prog"
    // /Users/eloipereira/Dropbox/Documents/workspace/Mealy/src/com/eloipereira/mealy/MealyMachine.g:21:1: prog returns [String outputWord] : 'Initial State = ' s= state ';' 'Transition Function = {' t= trMap '};' 'Output Function = {' o= outMap '};' 'Input word = ' w= inputWord ';' ;
    public final MealyMachineParser.prog_return prog() throws RecognitionException {
        MealyMachineParser.prog_return retval = new MealyMachineParser.prog_return();
        retval.start = input.LT(1);


        CommonTree root_0 = null;

        Token string_literal1=null;
        Token char_literal2=null;
        Token string_literal3=null;
        Token string_literal4=null;
        Token string_literal5=null;
        Token string_literal6=null;
        Token string_literal7=null;
        Token char_literal8=null;
        MealyMachineParser.state_return s =null;

        MealyMachineParser.trMap_return t =null;

        MealyMachineParser.outMap_return o =null;

        MealyMachineParser.inputWord_return w =null;


        CommonTree string_literal1_tree=null;
        CommonTree char_literal2_tree=null;
        CommonTree string_literal3_tree=null;
        CommonTree string_literal4_tree=null;
        CommonTree string_literal5_tree=null;
        CommonTree string_literal6_tree=null;
        CommonTree string_literal7_tree=null;
        CommonTree char_literal8_tree=null;

        try {
            // /Users/eloipereira/Dropbox/Documents/workspace/Mealy/src/com/eloipereira/mealy/MealyMachine.g:21:33: ( 'Initial State = ' s= state ';' 'Transition Function = {' t= trMap '};' 'Output Function = {' o= outMap '};' 'Input word = ' w= inputWord ';' )
            // /Users/eloipereira/Dropbox/Documents/workspace/Mealy/src/com/eloipereira/mealy/MealyMachine.g:22:2: 'Initial State = ' s= state ';' 'Transition Function = {' t= trMap '};' 'Output Function = {' o= outMap '};' 'Input word = ' w= inputWord ';'
            {
            root_0 = (CommonTree)adaptor.nil();


            string_literal1=(Token)match(input,13,FOLLOW_13_in_prog64); 
            string_literal1_tree = 
            (CommonTree)adaptor.create(string_literal1)
            ;
            adaptor.addChild(root_0, string_literal1_tree);


            pushFollow(FOLLOW_state_in_prog67);
            s=state();

            state._fsp--;

            adaptor.addChild(root_0, s.getTree());

            char_literal2=(Token)match(input,12,FOLLOW_12_in_prog69); 
            char_literal2_tree = 
            (CommonTree)adaptor.create(char_literal2)
            ;
            adaptor.addChild(root_0, char_literal2_tree);


            string_literal3=(Token)match(input,16,FOLLOW_16_in_prog73); 
            string_literal3_tree = 
            (CommonTree)adaptor.create(string_literal3)
            ;
            adaptor.addChild(root_0, string_literal3_tree);


            pushFollow(FOLLOW_trMap_in_prog77);
            t=trMap();

            state._fsp--;

            adaptor.addChild(root_0, t.getTree());

            string_literal4=(Token)match(input,17,FOLLOW_17_in_prog79); 
            string_literal4_tree = 
            (CommonTree)adaptor.create(string_literal4)
            ;
            adaptor.addChild(root_0, string_literal4_tree);


            string_literal5=(Token)match(input,15,FOLLOW_15_in_prog83); 
            string_literal5_tree = 
            (CommonTree)adaptor.create(string_literal5)
            ;
            adaptor.addChild(root_0, string_literal5_tree);


            pushFollow(FOLLOW_outMap_in_prog86);
            o=outMap();

            state._fsp--;

            adaptor.addChild(root_0, o.getTree());

            string_literal6=(Token)match(input,17,FOLLOW_17_in_prog88); 
            string_literal6_tree = 
            (CommonTree)adaptor.create(string_literal6)
            ;
            adaptor.addChild(root_0, string_literal6_tree);


            string_literal7=(Token)match(input,14,FOLLOW_14_in_prog92); 
            string_literal7_tree = 
            (CommonTree)adaptor.create(string_literal7)
            ;
            adaptor.addChild(root_0, string_literal7_tree);


            pushFollow(FOLLOW_inputWord_in_prog95);
            w=inputWord();

            state._fsp--;

            adaptor.addChild(root_0, w.getTree());

            char_literal8=(Token)match(input,12,FOLLOW_12_in_prog97); 
            char_literal8_tree = 
            (CommonTree)adaptor.create(char_literal8)
            ;
            adaptor.addChild(root_0, char_literal8_tree);



            		Mealy myMealy = new Mealy((s!=null?s.state:null), (t!=null?t.tr:null), (o!=null?o.out:null));
            		retval.outputWord = myMealy.execute((w!=null?w.word:null));
            	

            }

            retval.stop = input.LT(-1);


            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "prog"


    public static class state_return extends ParserRuleReturnScope {
        public Integer state;
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "state"
    // /Users/eloipereira/Dropbox/Documents/workspace/Mealy/src/com/eloipereira/mealy/MealyMachine.g:32:1: state returns [Integer state] : INT ;
    public final MealyMachineParser.state_return state() throws RecognitionException {
        MealyMachineParser.state_return retval = new MealyMachineParser.state_return();
        retval.start = input.LT(1);


        CommonTree root_0 = null;

        Token INT9=null;

        CommonTree INT9_tree=null;

        try {
            // /Users/eloipereira/Dropbox/Documents/workspace/Mealy/src/com/eloipereira/mealy/MealyMachine.g:32:30: ( INT )
            // /Users/eloipereira/Dropbox/Documents/workspace/Mealy/src/com/eloipereira/mealy/MealyMachine.g:33:2: INT
            {
            root_0 = (CommonTree)adaptor.nil();


            INT9=(Token)match(input,INT,FOLLOW_INT_in_state117); 
            INT9_tree = 
            (CommonTree)adaptor.create(INT9)
            ;
            adaptor.addChild(root_0, INT9_tree);


            retval.state=Integer.parseInt((INT9!=null?INT9.getText():null));

            }

            retval.stop = input.LT(-1);


            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "state"


    public static class trMap_return extends ParserRuleReturnScope {
        public HashMap<Tuple<Integer, Character>, Integer> tr;
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "trMap"
    // /Users/eloipereira/Dropbox/Documents/workspace/Mealy/src/com/eloipereira/mealy/MealyMachine.g:35:1: trMap returns [HashMap<Tuple<Integer, Character>, Integer> tr] : ( '(' s0= state ',' i= input ')' '->' s1= state )* ;
    public final MealyMachineParser.trMap_return trMap() throws RecognitionException {
        MealyMachineParser.trMap_return retval = new MealyMachineParser.trMap_return();
        retval.start = input.LT(1);


        CommonTree root_0 = null;

        Token char_literal10=null;
        Token char_literal11=null;
        Token char_literal12=null;
        Token string_literal13=null;
        MealyMachineParser.state_return s0 =null;

        MealyMachineParser.input_return i =null;

        MealyMachineParser.state_return s1 =null;


        CommonTree char_literal10_tree=null;
        CommonTree char_literal11_tree=null;
        CommonTree char_literal12_tree=null;
        CommonTree string_literal13_tree=null;

        retval.tr= new HashMap<Tuple<Integer, Character>, Integer>();
        try {
            // /Users/eloipereira/Dropbox/Documents/workspace/Mealy/src/com/eloipereira/mealy/MealyMachine.g:36:70: ( ( '(' s0= state ',' i= input ')' '->' s1= state )* )
            // /Users/eloipereira/Dropbox/Documents/workspace/Mealy/src/com/eloipereira/mealy/MealyMachine.g:37:2: ( '(' s0= state ',' i= input ')' '->' s1= state )*
            {
            root_0 = (CommonTree)adaptor.nil();


            // /Users/eloipereira/Dropbox/Documents/workspace/Mealy/src/com/eloipereira/mealy/MealyMachine.g:37:2: ( '(' s0= state ',' i= input ')' '->' s1= state )*
            loop1:
            do {
                int alt1=2;
                int LA1_0 = input.LA(1);

                if ( (LA1_0==8) ) {
                    alt1=1;
                }


                switch (alt1) {
            	case 1 :
            	    // /Users/eloipereira/Dropbox/Documents/workspace/Mealy/src/com/eloipereira/mealy/MealyMachine.g:37:3: '(' s0= state ',' i= input ')' '->' s1= state
            	    {
            	    char_literal10=(Token)match(input,8,FOLLOW_8_in_trMap139); 
            	    char_literal10_tree = 
            	    (CommonTree)adaptor.create(char_literal10)
            	    ;
            	    adaptor.addChild(root_0, char_literal10_tree);


            	    pushFollow(FOLLOW_state_in_trMap143);
            	    s0=state();

            	    state._fsp--;

            	    adaptor.addChild(root_0, s0.getTree());

            	    char_literal11=(Token)match(input,10,FOLLOW_10_in_trMap145); 
            	    char_literal11_tree = 
            	    (CommonTree)adaptor.create(char_literal11)
            	    ;
            	    adaptor.addChild(root_0, char_literal11_tree);


            	    pushFollow(FOLLOW_input_in_trMap149);
            	    i=input();

            	    state._fsp--;

            	    adaptor.addChild(root_0, i.getTree());

            	    char_literal12=(Token)match(input,9,FOLLOW_9_in_trMap151); 
            	    char_literal12_tree = 
            	    (CommonTree)adaptor.create(char_literal12)
            	    ;
            	    adaptor.addChild(root_0, char_literal12_tree);


            	    string_literal13=(Token)match(input,11,FOLLOW_11_in_trMap153); 
            	    string_literal13_tree = 
            	    (CommonTree)adaptor.create(string_literal13)
            	    ;
            	    adaptor.addChild(root_0, string_literal13_tree);


            	    pushFollow(FOLLOW_state_in_trMap157);
            	    s1=state();

            	    state._fsp--;

            	    adaptor.addChild(root_0, s1.getTree());


            	    		retval.tr.put(new Tuple<Integer, Character>((s0!=null?s0.state:null), (i!=null?i.input:null)), (s1!=null?s1.state:null));	
            	    	

            	    }
            	    break;

            	default :
            	    break loop1;
                }
            } while (true);


            }

            retval.stop = input.LT(-1);


            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "trMap"


    public static class outMap_return extends ParserRuleReturnScope {
        public HashMap<Tuple<Integer, Character>, Character> out;
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "outMap"
    // /Users/eloipereira/Dropbox/Documents/workspace/Mealy/src/com/eloipereira/mealy/MealyMachine.g:42:1: outMap returns [HashMap<Tuple<Integer, Character>, Character> out] : ( '(' s= state ',' i= input ')' '->' o= output )* ;
    public final MealyMachineParser.outMap_return outMap() throws RecognitionException {
        MealyMachineParser.outMap_return retval = new MealyMachineParser.outMap_return();
        retval.start = input.LT(1);


        CommonTree root_0 = null;

        Token char_literal14=null;
        Token char_literal15=null;
        Token char_literal16=null;
        Token string_literal17=null;
        MealyMachineParser.state_return s =null;

        MealyMachineParser.input_return i =null;

        MealyMachineParser.output_return o =null;


        CommonTree char_literal14_tree=null;
        CommonTree char_literal15_tree=null;
        CommonTree char_literal16_tree=null;
        CommonTree string_literal17_tree=null;

        retval.out= new HashMap<Tuple<Integer, Character>, Character>();
        try {
            // /Users/eloipereira/Dropbox/Documents/workspace/Mealy/src/com/eloipereira/mealy/MealyMachine.g:43:73: ( ( '(' s= state ',' i= input ')' '->' o= output )* )
            // /Users/eloipereira/Dropbox/Documents/workspace/Mealy/src/com/eloipereira/mealy/MealyMachine.g:44:2: ( '(' s= state ',' i= input ')' '->' o= output )*
            {
            root_0 = (CommonTree)adaptor.nil();


            // /Users/eloipereira/Dropbox/Documents/workspace/Mealy/src/com/eloipereira/mealy/MealyMachine.g:44:2: ( '(' s= state ',' i= input ')' '->' o= output )*
            loop2:
            do {
                int alt2=2;
                int LA2_0 = input.LA(1);

                if ( (LA2_0==8) ) {
                    alt2=1;
                }


                switch (alt2) {
            	case 1 :
            	    // /Users/eloipereira/Dropbox/Documents/workspace/Mealy/src/com/eloipereira/mealy/MealyMachine.g:44:3: '(' s= state ',' i= input ')' '->' o= output
            	    {
            	    char_literal14=(Token)match(input,8,FOLLOW_8_in_outMap182); 
            	    char_literal14_tree = 
            	    (CommonTree)adaptor.create(char_literal14)
            	    ;
            	    adaptor.addChild(root_0, char_literal14_tree);


            	    pushFollow(FOLLOW_state_in_outMap186);
            	    s=state();

            	    state._fsp--;

            	    adaptor.addChild(root_0, s.getTree());

            	    char_literal15=(Token)match(input,10,FOLLOW_10_in_outMap188); 
            	    char_literal15_tree = 
            	    (CommonTree)adaptor.create(char_literal15)
            	    ;
            	    adaptor.addChild(root_0, char_literal15_tree);


            	    pushFollow(FOLLOW_input_in_outMap192);
            	    i=input();

            	    state._fsp--;

            	    adaptor.addChild(root_0, i.getTree());

            	    char_literal16=(Token)match(input,9,FOLLOW_9_in_outMap194); 
            	    char_literal16_tree = 
            	    (CommonTree)adaptor.create(char_literal16)
            	    ;
            	    adaptor.addChild(root_0, char_literal16_tree);


            	    string_literal17=(Token)match(input,11,FOLLOW_11_in_outMap196); 
            	    string_literal17_tree = 
            	    (CommonTree)adaptor.create(string_literal17)
            	    ;
            	    adaptor.addChild(root_0, string_literal17_tree);


            	    pushFollow(FOLLOW_output_in_outMap200);
            	    o=output();

            	    state._fsp--;

            	    adaptor.addChild(root_0, o.getTree());


            	    		retval.out.put(new Tuple<Integer, Character>((s!=null?s.state:null), (i!=null?i.input:null)), (o!=null?o.output:null));
            	    	

            	    }
            	    break;

            	default :
            	    break loop2;
                }
            } while (true);


            }

            retval.stop = input.LT(-1);


            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "outMap"


    public static class inputWord_return extends ParserRuleReturnScope {
        public String word;
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "inputWord"
    // /Users/eloipereira/Dropbox/Documents/workspace/Mealy/src/com/eloipereira/mealy/MealyMachine.g:50:1: inputWord returns [String word] : STRING ;
    public final MealyMachineParser.inputWord_return inputWord() throws RecognitionException {
        MealyMachineParser.inputWord_return retval = new MealyMachineParser.inputWord_return();
        retval.start = input.LT(1);


        CommonTree root_0 = null;

        Token STRING18=null;

        CommonTree STRING18_tree=null;

        try {
            // /Users/eloipereira/Dropbox/Documents/workspace/Mealy/src/com/eloipereira/mealy/MealyMachine.g:50:32: ( STRING )
            // /Users/eloipereira/Dropbox/Documents/workspace/Mealy/src/com/eloipereira/mealy/MealyMachine.g:51:2: STRING
            {
            root_0 = (CommonTree)adaptor.nil();


            STRING18=(Token)match(input,STRING,FOLLOW_STRING_in_inputWord222); 
            STRING18_tree = 
            (CommonTree)adaptor.create(STRING18)
            ;
            adaptor.addChild(root_0, STRING18_tree);


            retval.word = (STRING18!=null?STRING18.getText():null);

            }

            retval.stop = input.LT(-1);


            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "inputWord"


    public static class input_return extends ParserRuleReturnScope {
        public Character input;
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "input"
    // /Users/eloipereira/Dropbox/Documents/workspace/Mealy/src/com/eloipereira/mealy/MealyMachine.g:53:1: input returns [Character input] : CHAR ;
    public final MealyMachineParser.input_return input() throws RecognitionException {
        MealyMachineParser.input_return retval = new MealyMachineParser.input_return();
        retval.start = input.LT(1);


        CommonTree root_0 = null;

        Token CHAR19=null;

        CommonTree CHAR19_tree=null;

        try {
            // /Users/eloipereira/Dropbox/Documents/workspace/Mealy/src/com/eloipereira/mealy/MealyMachine.g:53:32: ( CHAR )
            // /Users/eloipereira/Dropbox/Documents/workspace/Mealy/src/com/eloipereira/mealy/MealyMachine.g:54:2: CHAR
            {
            root_0 = (CommonTree)adaptor.nil();


            CHAR19=(Token)match(input,CHAR,FOLLOW_CHAR_in_input238); 
            CHAR19_tree = 
            (CommonTree)adaptor.create(CHAR19)
            ;
            adaptor.addChild(root_0, CHAR19_tree);


            retval.input = (CHAR19!=null?CHAR19.getText():null).charAt(0);

            }

            retval.stop = input.LT(-1);


            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "input"


    public static class output_return extends ParserRuleReturnScope {
        public Character output;
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "output"
    // /Users/eloipereira/Dropbox/Documents/workspace/Mealy/src/com/eloipereira/mealy/MealyMachine.g:56:1: output returns [Character output] : CHAR ;
    public final MealyMachineParser.output_return output() throws RecognitionException {
        MealyMachineParser.output_return retval = new MealyMachineParser.output_return();
        retval.start = input.LT(1);


        CommonTree root_0 = null;

        Token CHAR20=null;

        CommonTree CHAR20_tree=null;

        try {
            // /Users/eloipereira/Dropbox/Documents/workspace/Mealy/src/com/eloipereira/mealy/MealyMachine.g:56:34: ( CHAR )
            // /Users/eloipereira/Dropbox/Documents/workspace/Mealy/src/com/eloipereira/mealy/MealyMachine.g:57:2: CHAR
            {
            root_0 = (CommonTree)adaptor.nil();


            CHAR20=(Token)match(input,CHAR,FOLLOW_CHAR_in_output254); 
            CHAR20_tree = 
            (CommonTree)adaptor.create(CHAR20)
            ;
            adaptor.addChild(root_0, CHAR20_tree);


            retval.output = (CHAR20!=null?CHAR20.getText():null).charAt(0);

            }

            retval.stop = input.LT(-1);


            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "output"

    // Delegated rules


 

    public static final BitSet FOLLOW_13_in_prog64 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_state_in_prog67 = new BitSet(new long[]{0x0000000000001000L});
    public static final BitSet FOLLOW_12_in_prog69 = new BitSet(new long[]{0x0000000000010000L});
    public static final BitSet FOLLOW_16_in_prog73 = new BitSet(new long[]{0x0000000000020100L});
    public static final BitSet FOLLOW_trMap_in_prog77 = new BitSet(new long[]{0x0000000000020000L});
    public static final BitSet FOLLOW_17_in_prog79 = new BitSet(new long[]{0x0000000000008000L});
    public static final BitSet FOLLOW_15_in_prog83 = new BitSet(new long[]{0x0000000000020100L});
    public static final BitSet FOLLOW_outMap_in_prog86 = new BitSet(new long[]{0x0000000000020000L});
    public static final BitSet FOLLOW_17_in_prog88 = new BitSet(new long[]{0x0000000000004000L});
    public static final BitSet FOLLOW_14_in_prog92 = new BitSet(new long[]{0x0000000000000040L});
    public static final BitSet FOLLOW_inputWord_in_prog95 = new BitSet(new long[]{0x0000000000001000L});
    public static final BitSet FOLLOW_12_in_prog97 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_INT_in_state117 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_8_in_trMap139 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_state_in_trMap143 = new BitSet(new long[]{0x0000000000000400L});
    public static final BitSet FOLLOW_10_in_trMap145 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_input_in_trMap149 = new BitSet(new long[]{0x0000000000000200L});
    public static final BitSet FOLLOW_9_in_trMap151 = new BitSet(new long[]{0x0000000000000800L});
    public static final BitSet FOLLOW_11_in_trMap153 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_state_in_trMap157 = new BitSet(new long[]{0x0000000000000102L});
    public static final BitSet FOLLOW_8_in_outMap182 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_state_in_outMap186 = new BitSet(new long[]{0x0000000000000400L});
    public static final BitSet FOLLOW_10_in_outMap188 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_input_in_outMap192 = new BitSet(new long[]{0x0000000000000200L});
    public static final BitSet FOLLOW_9_in_outMap194 = new BitSet(new long[]{0x0000000000000800L});
    public static final BitSet FOLLOW_11_in_outMap196 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_output_in_outMap200 = new BitSet(new long[]{0x0000000000000102L});
    public static final BitSet FOLLOW_STRING_in_inputWord222 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_CHAR_in_input238 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_CHAR_in_output254 = new BitSet(new long[]{0x0000000000000002L});

}