package com.eloipereira.mealy;

import java.io.IOException;

import org.antlr.runtime.ANTLRFileStream;
import org.antlr.runtime.CharStream;
import org.antlr.runtime.CommonTokenStream;
import org.antlr.runtime.RecognitionException;

public class TestMealyLang {
	public static void main(String[] args) throws RecognitionException, IOException {
		CharStream input = new ANTLRFileStream("/Users/eloipereira/Dropbox/Documents/workspace/Mealy/src/com/eloipereira/mealy/simple.mealy");
		MealyMachineLexer lexer = new MealyMachineLexer(input);
		CommonTokenStream tokens = new CommonTokenStream(lexer);
		MealyMachineParser parser = new MealyMachineParser(tokens);
		System.out.println(parser.prog().outputWord);
	}
}
