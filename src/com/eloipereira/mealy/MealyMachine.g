grammar MealyMachine;

options {language = Java;
	 output = AST;
  	 ASTLabelType=CommonTree;
} 

@header {
  package com.eloipereira.mealy;
  import java.util.HashMap;
}

@lexer::header{
  package com.eloipereira.mealy;
}

@members{
  private String word = new String(); 
}

prog returns [String outputWord]:	
	'Initial State = 's=state ';' 
	'Transition Function = {' t=trMap '};' 
	'Output Function = {'o=outMap '};' 
	'Input word = 'w=inputWord ';'
	{
		Mealy myMealy = new Mealy($s.state, $t.tr, $o.out);
		retval.outputWord = myMealy.execute($w.word);
	}
	;
	
state returns [Integer state]:		
	INT {retval.state=Integer.parseInt($INT.text);};
	
trMap returns [HashMap<Tuple<Integer, Character>, Integer> tr]
@init {retval.tr= new HashMap<Tuple<Integer, Character>, Integer>();}:	
	('(' s0=state ',' i=input ')' '->' s1=state
	{
		retval.tr.put(new Tuple<Integer, Character>($s0.state, $i.input), $s1.state);	
	})*;
	
outMap returns [HashMap<Tuple<Integer, Character>, Character> out]
@init {retval.out= new HashMap<Tuple<Integer, Character>, Character>();}:	
	('(' s=state ',' i=input ')' '->' o=output
	{
		retval.out.put(new Tuple<Integer, Character>($s.state, $i.input), $o.output);
	}	
	)*;
	
inputWord returns [String word]:	
	STRING {retval.word = $STRING.text;};
	
input returns [Character input]:	
	CHAR {retval.input = $CHAR.text.charAt(0);};
	
output returns [Character output]:	
	CHAR {retval.output = $CHAR.text.charAt(0);};


CHAR 	: 'a'..'z';
INT 	: '0'..'9'+;
STRING: ('a'..'z')* ;
WS  	: ( ' ' | '\t' | '\r' | '\n') {$channel=HIDDEN;};


