// $ANTLR 3.4 /Users/eloipereira/Dropbox/Documents/workspace/Mealy/src/com/eloipereira/mealy/MealyMachine.g 2013-10-23 00:05:14

  package com.eloipereira.mealy;


import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings({"all", "warnings", "unchecked"})
public class MealyMachineLexer extends Lexer {
    public static final int EOF=-1;
    public static final int T__8=8;
    public static final int T__9=9;
    public static final int T__10=10;
    public static final int T__11=11;
    public static final int T__12=12;
    public static final int T__13=13;
    public static final int T__14=14;
    public static final int T__15=15;
    public static final int T__16=16;
    public static final int T__17=17;
    public static final int CHAR=4;
    public static final int INT=5;
    public static final int STRING=6;
    public static final int WS=7;

    // delegates
    // delegators
    public Lexer[] getDelegates() {
        return new Lexer[] {};
    }

    public MealyMachineLexer() {} 
    public MealyMachineLexer(CharStream input) {
        this(input, new RecognizerSharedState());
    }
    public MealyMachineLexer(CharStream input, RecognizerSharedState state) {
        super(input,state);
    }
    public String getGrammarFileName() { return "/Users/eloipereira/Dropbox/Documents/workspace/Mealy/src/com/eloipereira/mealy/MealyMachine.g"; }

    // $ANTLR start "T__8"
    public final void mT__8() throws RecognitionException {
        try {
            int _type = T__8;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /Users/eloipereira/Dropbox/Documents/workspace/Mealy/src/com/eloipereira/mealy/MealyMachine.g:11:6: ( '(' )
            // /Users/eloipereira/Dropbox/Documents/workspace/Mealy/src/com/eloipereira/mealy/MealyMachine.g:11:8: '('
            {
            match('('); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__8"

    // $ANTLR start "T__9"
    public final void mT__9() throws RecognitionException {
        try {
            int _type = T__9;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /Users/eloipereira/Dropbox/Documents/workspace/Mealy/src/com/eloipereira/mealy/MealyMachine.g:12:6: ( ')' )
            // /Users/eloipereira/Dropbox/Documents/workspace/Mealy/src/com/eloipereira/mealy/MealyMachine.g:12:8: ')'
            {
            match(')'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__9"

    // $ANTLR start "T__10"
    public final void mT__10() throws RecognitionException {
        try {
            int _type = T__10;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /Users/eloipereira/Dropbox/Documents/workspace/Mealy/src/com/eloipereira/mealy/MealyMachine.g:13:7: ( ',' )
            // /Users/eloipereira/Dropbox/Documents/workspace/Mealy/src/com/eloipereira/mealy/MealyMachine.g:13:9: ','
            {
            match(','); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__10"

    // $ANTLR start "T__11"
    public final void mT__11() throws RecognitionException {
        try {
            int _type = T__11;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /Users/eloipereira/Dropbox/Documents/workspace/Mealy/src/com/eloipereira/mealy/MealyMachine.g:14:7: ( '->' )
            // /Users/eloipereira/Dropbox/Documents/workspace/Mealy/src/com/eloipereira/mealy/MealyMachine.g:14:9: '->'
            {
            match("->"); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__11"

    // $ANTLR start "T__12"
    public final void mT__12() throws RecognitionException {
        try {
            int _type = T__12;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /Users/eloipereira/Dropbox/Documents/workspace/Mealy/src/com/eloipereira/mealy/MealyMachine.g:15:7: ( ';' )
            // /Users/eloipereira/Dropbox/Documents/workspace/Mealy/src/com/eloipereira/mealy/MealyMachine.g:15:9: ';'
            {
            match(';'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__12"

    // $ANTLR start "T__13"
    public final void mT__13() throws RecognitionException {
        try {
            int _type = T__13;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /Users/eloipereira/Dropbox/Documents/workspace/Mealy/src/com/eloipereira/mealy/MealyMachine.g:16:7: ( 'Initial State = ' )
            // /Users/eloipereira/Dropbox/Documents/workspace/Mealy/src/com/eloipereira/mealy/MealyMachine.g:16:9: 'Initial State = '
            {
            match("Initial State = "); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__13"

    // $ANTLR start "T__14"
    public final void mT__14() throws RecognitionException {
        try {
            int _type = T__14;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /Users/eloipereira/Dropbox/Documents/workspace/Mealy/src/com/eloipereira/mealy/MealyMachine.g:17:7: ( 'Input word = ' )
            // /Users/eloipereira/Dropbox/Documents/workspace/Mealy/src/com/eloipereira/mealy/MealyMachine.g:17:9: 'Input word = '
            {
            match("Input word = "); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__14"

    // $ANTLR start "T__15"
    public final void mT__15() throws RecognitionException {
        try {
            int _type = T__15;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /Users/eloipereira/Dropbox/Documents/workspace/Mealy/src/com/eloipereira/mealy/MealyMachine.g:18:7: ( 'Output Function = {' )
            // /Users/eloipereira/Dropbox/Documents/workspace/Mealy/src/com/eloipereira/mealy/MealyMachine.g:18:9: 'Output Function = {'
            {
            match("Output Function = {"); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__15"

    // $ANTLR start "T__16"
    public final void mT__16() throws RecognitionException {
        try {
            int _type = T__16;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /Users/eloipereira/Dropbox/Documents/workspace/Mealy/src/com/eloipereira/mealy/MealyMachine.g:19:7: ( 'Transition Function = {' )
            // /Users/eloipereira/Dropbox/Documents/workspace/Mealy/src/com/eloipereira/mealy/MealyMachine.g:19:9: 'Transition Function = {'
            {
            match("Transition Function = {"); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__16"

    // $ANTLR start "T__17"
    public final void mT__17() throws RecognitionException {
        try {
            int _type = T__17;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /Users/eloipereira/Dropbox/Documents/workspace/Mealy/src/com/eloipereira/mealy/MealyMachine.g:20:7: ( '};' )
            // /Users/eloipereira/Dropbox/Documents/workspace/Mealy/src/com/eloipereira/mealy/MealyMachine.g:20:9: '};'
            {
            match("};"); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__17"

    // $ANTLR start "CHAR"
    public final void mCHAR() throws RecognitionException {
        try {
            int _type = CHAR;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /Users/eloipereira/Dropbox/Documents/workspace/Mealy/src/com/eloipereira/mealy/MealyMachine.g:60:7: ( 'a' .. 'z' )
            // /Users/eloipereira/Dropbox/Documents/workspace/Mealy/src/com/eloipereira/mealy/MealyMachine.g:
            {
            if ( (input.LA(1) >= 'a' && input.LA(1) <= 'z') ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "CHAR"

    // $ANTLR start "INT"
    public final void mINT() throws RecognitionException {
        try {
            int _type = INT;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /Users/eloipereira/Dropbox/Documents/workspace/Mealy/src/com/eloipereira/mealy/MealyMachine.g:61:6: ( ( '0' .. '9' )+ )
            // /Users/eloipereira/Dropbox/Documents/workspace/Mealy/src/com/eloipereira/mealy/MealyMachine.g:61:8: ( '0' .. '9' )+
            {
            // /Users/eloipereira/Dropbox/Documents/workspace/Mealy/src/com/eloipereira/mealy/MealyMachine.g:61:8: ( '0' .. '9' )+
            int cnt1=0;
            loop1:
            do {
                int alt1=2;
                int LA1_0 = input.LA(1);

                if ( ((LA1_0 >= '0' && LA1_0 <= '9')) ) {
                    alt1=1;
                }


                switch (alt1) {
            	case 1 :
            	    // /Users/eloipereira/Dropbox/Documents/workspace/Mealy/src/com/eloipereira/mealy/MealyMachine.g:
            	    {
            	    if ( (input.LA(1) >= '0' && input.LA(1) <= '9') ) {
            	        input.consume();
            	    }
            	    else {
            	        MismatchedSetException mse = new MismatchedSetException(null,input);
            	        recover(mse);
            	        throw mse;
            	    }


            	    }
            	    break;

            	default :
            	    if ( cnt1 >= 1 ) break loop1;
                        EarlyExitException eee =
                            new EarlyExitException(1, input);
                        throw eee;
                }
                cnt1++;
            } while (true);


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "INT"

    // $ANTLR start "STRING"
    public final void mSTRING() throws RecognitionException {
        try {
            int _type = STRING;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /Users/eloipereira/Dropbox/Documents/workspace/Mealy/src/com/eloipereira/mealy/MealyMachine.g:62:7: ( ( 'a' .. 'z' )* )
            // /Users/eloipereira/Dropbox/Documents/workspace/Mealy/src/com/eloipereira/mealy/MealyMachine.g:62:9: ( 'a' .. 'z' )*
            {
            // /Users/eloipereira/Dropbox/Documents/workspace/Mealy/src/com/eloipereira/mealy/MealyMachine.g:62:9: ( 'a' .. 'z' )*
            loop2:
            do {
                int alt2=2;
                int LA2_0 = input.LA(1);

                if ( ((LA2_0 >= 'a' && LA2_0 <= 'z')) ) {
                    alt2=1;
                }


                switch (alt2) {
            	case 1 :
            	    // /Users/eloipereira/Dropbox/Documents/workspace/Mealy/src/com/eloipereira/mealy/MealyMachine.g:
            	    {
            	    if ( (input.LA(1) >= 'a' && input.LA(1) <= 'z') ) {
            	        input.consume();
            	    }
            	    else {
            	        MismatchedSetException mse = new MismatchedSetException(null,input);
            	        recover(mse);
            	        throw mse;
            	    }


            	    }
            	    break;

            	default :
            	    break loop2;
                }
            } while (true);


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "STRING"

    // $ANTLR start "WS"
    public final void mWS() throws RecognitionException {
        try {
            int _type = WS;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /Users/eloipereira/Dropbox/Documents/workspace/Mealy/src/com/eloipereira/mealy/MealyMachine.g:63:6: ( ( ' ' | '\\t' | '\\r' | '\\n' ) )
            // /Users/eloipereira/Dropbox/Documents/workspace/Mealy/src/com/eloipereira/mealy/MealyMachine.g:63:8: ( ' ' | '\\t' | '\\r' | '\\n' )
            {
            if ( (input.LA(1) >= '\t' && input.LA(1) <= '\n')||input.LA(1)=='\r'||input.LA(1)==' ' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            _channel=HIDDEN;

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "WS"

    public void mTokens() throws RecognitionException {
        // /Users/eloipereira/Dropbox/Documents/workspace/Mealy/src/com/eloipereira/mealy/MealyMachine.g:1:8: ( T__8 | T__9 | T__10 | T__11 | T__12 | T__13 | T__14 | T__15 | T__16 | T__17 | CHAR | INT | STRING | WS )
        int alt3=14;
        switch ( input.LA(1) ) {
        case '(':
            {
            alt3=1;
            }
            break;
        case ')':
            {
            alt3=2;
            }
            break;
        case ',':
            {
            alt3=3;
            }
            break;
        case '-':
            {
            alt3=4;
            }
            break;
        case ';':
            {
            alt3=5;
            }
            break;
        case 'I':
            {
            int LA3_6 = input.LA(2);

            if ( (LA3_6=='n') ) {
                int LA3_14 = input.LA(3);

                if ( (LA3_14=='i') ) {
                    alt3=6;
                }
                else if ( (LA3_14=='p') ) {
                    alt3=7;
                }
                else {
                    NoViableAltException nvae =
                        new NoViableAltException("", 3, 14, input);

                    throw nvae;

                }
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 3, 6, input);

                throw nvae;

            }
            }
            break;
        case 'O':
            {
            alt3=8;
            }
            break;
        case 'T':
            {
            alt3=9;
            }
            break;
        case '}':
            {
            alt3=10;
            }
            break;
        case 'a':
        case 'b':
        case 'c':
        case 'd':
        case 'e':
        case 'f':
        case 'g':
        case 'h':
        case 'i':
        case 'j':
        case 'k':
        case 'l':
        case 'm':
        case 'n':
        case 'o':
        case 'p':
        case 'q':
        case 'r':
        case 's':
        case 't':
        case 'u':
        case 'v':
        case 'w':
        case 'x':
        case 'y':
        case 'z':
            {
            int LA3_10 = input.LA(2);

            if ( ((LA3_10 >= 'a' && LA3_10 <= 'z')) ) {
                alt3=13;
            }
            else {
                alt3=11;
            }
            }
            break;
        case '0':
        case '1':
        case '2':
        case '3':
        case '4':
        case '5':
        case '6':
        case '7':
        case '8':
        case '9':
            {
            alt3=12;
            }
            break;
        case '\t':
        case '\n':
        case '\r':
        case ' ':
            {
            alt3=14;
            }
            break;
        default:
            alt3=13;
        }

        switch (alt3) {
            case 1 :
                // /Users/eloipereira/Dropbox/Documents/workspace/Mealy/src/com/eloipereira/mealy/MealyMachine.g:1:10: T__8
                {
                mT__8(); 


                }
                break;
            case 2 :
                // /Users/eloipereira/Dropbox/Documents/workspace/Mealy/src/com/eloipereira/mealy/MealyMachine.g:1:15: T__9
                {
                mT__9(); 


                }
                break;
            case 3 :
                // /Users/eloipereira/Dropbox/Documents/workspace/Mealy/src/com/eloipereira/mealy/MealyMachine.g:1:20: T__10
                {
                mT__10(); 


                }
                break;
            case 4 :
                // /Users/eloipereira/Dropbox/Documents/workspace/Mealy/src/com/eloipereira/mealy/MealyMachine.g:1:26: T__11
                {
                mT__11(); 


                }
                break;
            case 5 :
                // /Users/eloipereira/Dropbox/Documents/workspace/Mealy/src/com/eloipereira/mealy/MealyMachine.g:1:32: T__12
                {
                mT__12(); 


                }
                break;
            case 6 :
                // /Users/eloipereira/Dropbox/Documents/workspace/Mealy/src/com/eloipereira/mealy/MealyMachine.g:1:38: T__13
                {
                mT__13(); 


                }
                break;
            case 7 :
                // /Users/eloipereira/Dropbox/Documents/workspace/Mealy/src/com/eloipereira/mealy/MealyMachine.g:1:44: T__14
                {
                mT__14(); 


                }
                break;
            case 8 :
                // /Users/eloipereira/Dropbox/Documents/workspace/Mealy/src/com/eloipereira/mealy/MealyMachine.g:1:50: T__15
                {
                mT__15(); 


                }
                break;
            case 9 :
                // /Users/eloipereira/Dropbox/Documents/workspace/Mealy/src/com/eloipereira/mealy/MealyMachine.g:1:56: T__16
                {
                mT__16(); 


                }
                break;
            case 10 :
                // /Users/eloipereira/Dropbox/Documents/workspace/Mealy/src/com/eloipereira/mealy/MealyMachine.g:1:62: T__17
                {
                mT__17(); 


                }
                break;
            case 11 :
                // /Users/eloipereira/Dropbox/Documents/workspace/Mealy/src/com/eloipereira/mealy/MealyMachine.g:1:68: CHAR
                {
                mCHAR(); 


                }
                break;
            case 12 :
                // /Users/eloipereira/Dropbox/Documents/workspace/Mealy/src/com/eloipereira/mealy/MealyMachine.g:1:73: INT
                {
                mINT(); 


                }
                break;
            case 13 :
                // /Users/eloipereira/Dropbox/Documents/workspace/Mealy/src/com/eloipereira/mealy/MealyMachine.g:1:77: STRING
                {
                mSTRING(); 


                }
                break;
            case 14 :
                // /Users/eloipereira/Dropbox/Documents/workspace/Mealy/src/com/eloipereira/mealy/MealyMachine.g:1:84: WS
                {
                mWS(); 


                }
                break;

        }

    }


 

}