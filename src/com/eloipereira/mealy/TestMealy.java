package com.eloipereira.mealy;

import java.util.HashMap;

public class TestMealy {
	public static void main(String[] args) {
	 HashMap<Tuple<Integer,Character>,Integer> transitionMap = new HashMap<Tuple<Integer,Character>, Integer>();
	 HashMap<Tuple<Integer,Character>,Character> outputMap = new HashMap<Tuple<Integer,Character>, Character>();
	 transitionMap.put(new Tuple<Integer, Character>(0, 'a'), 1);
	 transitionMap.put(new Tuple<Integer, Character>(1, 'b'), 0);
	 outputMap.put(new Tuple<Integer, Character>(0, 'a'), 'b');
	 outputMap.put(new Tuple<Integer, Character>(1, 'b'), 'a');

	 Mealy myMealy = new Mealy(0, transitionMap, outputMap);
	 System.out.println(myMealy.execute("abababab"));
	}
}
