package com.eloipereira.mealy;

import java.util.HashMap;

public class Mealy {
	private Integer state;
	private HashMap<Tuple<Integer,Character>,Integer> transitionMap;
	private HashMap<Tuple<Integer,Character>,Character> outputMap;

	public Mealy(Integer state,
			HashMap<Tuple<Integer, Character>, Integer> transitionMap,
			HashMap<Tuple<Integer, Character>, Character> outputMap) {
		this.state = state;
		this.transitionMap = transitionMap;
		this.outputMap = outputMap;
	}

	public String execute(String inputWord){
		String outputWord = "";
		if (!inputWord.isEmpty()){
			Character output = outputMap.get(new Tuple<Integer, Character>(state,(Character) inputWord.charAt(0)));
			this.state = transitionMap.get(new Tuple<Integer, Character>(state,(Character) inputWord.charAt(0)));
			outputWord += output + execute(inputWord.substring(1));
		}
		return outputWord;
	}
}
